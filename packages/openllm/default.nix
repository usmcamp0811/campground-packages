{
  config,
  lib,
  dream2nix,
  ...
}: let
  python = config.deps.python;
in {
  imports = [
    dream2nix.modules.dream2nix.pip
  ];

  deps = {
    nixpkgs,
    nixpkgsStable,
    ...
  }: {
    inherit
      (nixpkgs)
      openllm
      git
      fetchFromGitHub
      ;
    python = nixpkgs.python311;
  };

  name = "openllm";
  version = "v0.4.41";

  mkDerivation = {
    src = config.deps.fetchFromGitHub {
      owner = "bentoml";
      repo = "OpenLLM";
      rev = "refs/tags/${config.version}";
      # Download using the git protocol rather than using tarballs, because the
      # GitHub archive tarballs don't appear to include tests
      forceFetchGit = true;
      hash = "sha256-QWUXSG+RSHkF5kP1ZYtx+tHjO0n7hfya9CFA3lBhJHk=";
    };

    nativeBuildInputs = [
      python.pkgs.GitPython
    ];
  };

  # workaround because the setuptools hook propagates its inputs resulting in conflicts
  buildPythonPackage.catchConflicts = false;

  pip = {
    pypiSnapshotDate = "2024-01-27";
    pipVersion = "23.3.2";
    requirementsList = [
      "openllm"
    ];

  };
}
