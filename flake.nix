{
  description = "Flake of Packages that I use in the Campground";

  inputs = {
    dream2nix.url = "github:nix-community/dream2nix";
    nixpkgs.url = "github:nixos/nixpkgs/24.11";
    nixpkgs.follows = "dream2nix/nixpkgs";
  };

  outputs = { self, dream2nix, nixpkgs, ... }:
    let
      system = "x86_64-linux";

      # Import packages using dream2nix
      packages = dream2nix.lib.importPackages {
        projectRoot = ./.;
        projectRootFile =
          "flake.nix"; # Ensure this file exists and is correctly set up
        packagesDir =
          ./packages; # Ensure the directory and its contents are correctly set up
        packageSets.nixpkgs = nixpkgs.legacyPackages.${system};
      };
      pkgs = import nixpkgs { };

    in {
      # Expose all packages defined inside ./packages/
      packages.${system} = packages;

      # Set default package for easy access
      defaultPackage.${system} =
        packages.label-studio; # Ensure `label-studio` is correctly defined in `./packages`

      # Define a NixOS test for label-studio
      # checks.label-studio-test = pkgs.nixosTest {
      #   name = "label-studio-test";
      #   nodes = {
      #     machine = { ... }: {
      #       environment.systemPackages = [ packages.label-studio ];
      #     };
      #   };
      #   testScript = ''
      #     startAll;
      #     machine.waitUntilSucceeds("label-studio --help");
      #     machine.succeed("label-studio --help");
      #   '';
      # };
    };
}
