{ config, lib, dream2nix, ... }:
let
  label-studio-src = config.deps.fetchFromGitHub {
    owner = "HumanSignal";
    repo = "label-studio";
    rev = "a9766f0325bd99ff2339ffa8a7988a66383c0f8d";
    forceFetchGit = true;
    hash = "sha256-K9zszNvmNnd0x5Qy6naPCE69PnvHc0y4RhJ95InqzZg=";
  };
  core = builtins.path {
    name = "label-studio-with-core";
    path = ./.;
  };
in {
  imports = [
    dream2nix.modules.dream2nix.pip

  ];

  deps = { nixpkgs, nixpkgsStable, ... }: {
    inherit (nixpkgs)
      git fetchFromGitHub mysql postgresql dbus uwsgi python311Packages;
    python = nixpkgs.python311;
  };

  name = "label_studio";
  version = "1.11.0";

  buildPythonPackage = {
    catchConflicts = false;
    pythonImportsCheck = [ config.name ];
  };

  mkDerivation = {
    src = label-studio-src;
    preBuild = ''
      cd ./label_studio
      rm -rf core
      ls -lah ${core}
      cp -r ${core}/core ./core
      cd ..
      python label_studio/core/version.py
    '';
    postInstall = ''
      export PYTHONEXEC=$(python -c "import django; import sys; print(sys.executable)")
      mkdir -p $out/etc
      echo "#!/bin/sh" > $out/bin/label-studio-gunicorn
      echo "set -e" >> $out/bin/label-studio-gunicorn
      # set this LABEL_STUDIO_BASE_DATA_DIR=/label-studio/data \
      echo "export DJANGO_SETTINGS_MODULE=core.settings.label_studio" >> $out/bin/label-studio-gunicorn
      echo "export PYTHONPATH=$out/lib/python3.11/site-packages:$PYTHONPATH" >> $out/bin/label-studio-gunicorn
      echo "${config.deps.python311Packages.gunicorn}/bin/gunicorn --chdir $out/lib/python3.11/site-packages/label_studio core.wsgi:application \"\$@\"" >> $out/bin/label-studio-gunicorn  
      chmod +x $out/bin/label-studio-gunicorn
    '';
  };

  pip = {
    pipVersion = "24.0";
    pypiSnapshotDate = "2024-2-2";
    flattenDependencies = true;
    requirementsList = [
      "label-studio"
      "gunicorn"
      "setuptools-scm"
      "setuptools"
      "appdirs"
      "asgiref"
      "attrs"
      "azure-core"
      "azure-storage-blob"
      "bleach"
      "boto"
      "boto3"
      "botocore"
      "boxing"
      "cachetools"
      "certifi"
      "cffi"
      "charset-normalizer"
      "click"
      "colorama"
      "cryptography"
      "defusedxml"
      "Django"
      "django-annoying"
      "django-cors-headers"
      "django-debug-toolbar"
      "django-environ"
      "django-extensions"
      "django-filter"
      "django-model-utils"
      "django-ranged-fileresponse"
      "django-rq"
      "django-storages"
      "django-user-agents"
      "djangorestframework"
      "drf-dynamic-fields"
      "drf-flex-fields"
      "drf-generators"
      "expiringdict"
      "google-api-core"
      "google-auth"
      "google-cloud-appengine-logging"
      "google-cloud-audit-log"
      "google-cloud-core"
      "google-cloud-logging"
      "google-cloud-storage"
      "google-crc32c"
      "google-resumable-media"
      "googleapis-common-protos"
      "grpc-google-iam-v1"
      "grpcio"
      "grpcio-status"
      "htmlmin"
      # "humansignal-drf-yasg"
      "idna"
      "ijson"
      "inflection"
      "isodate"
      "jmespath"
      "joblib"
      # "jsonschema"
      "label-studio-converter"
      "label-studio-tools"
      "launchdarkly-server-sdk"
      "lockfile"
      "lxml"
      "nltk"
      "numpy"
      "ordered-set"
      "packaging"
      "pandas"
      "Pillow"
      "proto-plus"
      "protobuf"
      "psycopg2-binary"
      "pyasn1"
      "pyasn1-modules"
      "pycparser"
      "pydantic"
      "pyRFC3339"
      "pyrsistent"
      "python-dateutil"
      "python-json-logger"
      "pytz"
      "PyYAML"
      "redis"
      "regex"
      "requests"
      "rq"
      "rsa"
      "rules"
      # "s3transfer"
      "semver"
      "sentry-sdk"
      "six"
      "sqlparse"
      "tqdm"
      "typing_extensions"
      "tzdata"
      "ua-parser"
      "ujson"
      "uritemplate"
      "urllib3"
      "user-agents"
      "uWSGI"
      "uwsgitop"
      # "webencodings"
      "xmljson"
    ];
  };
}
