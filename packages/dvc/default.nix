{
  config,
  lib,
  dream2nix,
  ...
}: 
{
  imports = [
    dream2nix.modules.dream2nix.pip

  ];

  deps = {
    nixpkgs,
    nixpkgsStable,
    ...
  }: {
    inherit
      (nixpkgs)
      git
      fetchFromGitHub
      zlib
      libjpeg
      pkg-config

      ;
  };

  name = "dvc";
  version = "3.30.0";

  mkDerivation = {
    nativeBuildInputs = [
      config.deps.pkg-config
    ];
    propagatedBuildInputs = [
      config.deps.zlib
      config.deps.libjpeg
    ];
  };

  buildPythonPackage = {
    catchConflicts = true;
    pythonImportsCheck = [
      config.name
    ];
  };


  pip = {
    pipVersion = "23.3.1";
    pypiSnapshotDate = "2023-11-16";
    # flattenDependencies = false;
    requirementsList = [
      "dvc==3.30.0"
    ];
  };
}
